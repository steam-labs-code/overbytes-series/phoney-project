import random 
import string 
from pathlib import Path 


def phone_number() -> str:
    get_three = lambda: ''.join(random.choices(string.digits, k=3))
    fourth = f'{get_three()}{random.randint(1,9)}'

    return f'{get_three()}-{get_three()}-{fourth}'


def company_name() -> str:
    fake_company = f'{random.choice(city_names)} {random.choice(company_types)} {random.choice(finishers)}'

    return fake_company.title()


def email() -> str:
    return f'{first_name()}_{last_name()}@example.com'


def first_name() -> str:
    names_path = Path('lib/first_names.txt')
    with names_path.open() as file:
        names = file.readlines()

    return random.choice(names).strip()


def last_name() -> str:
    names_path = Path('lib/last_names.txt')
    with names_path.open() as file:
        names = file.readlines()

    return random.choice(names).strip()


def full_name() -> str:
    return f'{first_name()} {last_name()}'


def price(min_value: int=1, max_value: int=100, accy: int=2) -> float:
    return round(random.random()*max_value, accy)


def address(secondary: bool=False) -> str:
    """Return a street number and street name."""
    street_no = str(random.randint(1,99999))
    street_name = random.choice(lexicon).strip().title()
    if secondary:
        return f'{street_no} {street_name} {random.choice(street_types).capitalize()}, {random.choice(secondary_addr).capitalize()} {random.randint(1,100)}'
    return f'{street_no} {street_name} {random.choice(street_types).capitalize()}'


def person() -> dict:
    return {
        'first_name': first_name(),
        'last_name': last_name(),
        'address': address(),
        'phone': phone_number(),
        'email': email(),
        'company': company_name()
    }


def people(count: int=50):
    results = []
    for i in range(50):
        results.append(person())
    return results


if __name__ == '__main__':
    import sys 

    args = sys.argv[1:] 
    
    if args:
        if args[0] == 'phone':
            print(phone_number())
        elif args[0] == 'company':
            print(company_name())
        elif args[0] == 'email':
            print(email())
        elif args[0] == 'name':
            print(full_name())
        elif args[0] == 'price':
            print(price())
        elif args[0] == 'address':
            if len(args) > 1:
                print(address(secondary=True))
            else:
                print(address())
    else:
        print('To get a phoney, ask for: phone, email, company')
        print('Example: python3 app.py phone')

