# Phoney

A module that generates fake data! BE A PHONEY NOT A FAKER :D

> NOTE: Some text files used during the workshop might not be in the folders, but fear not -- you can make up your text files! 

**Phoneys**
- Phone numbers
- People
- Addresses
- Cities
- States
- Countries
- Company names
- Email
- Prices

Example use:
```python
import phoney

phoney.phone()
phoney.place()  # returns city and state
phoney.address()
```
